const countPositivesSumNegatives = (list) => {
  var count = 0
  var sum = 0

  if(!list.length)
      return []
  for(var i=0; i<list.length; i++){
      if(list[i]>0){
        count++
      }else{
        sum+=list[i]
      }
  }

  return [count,sum]
}

module.exports = countPositivesSumNegatives
